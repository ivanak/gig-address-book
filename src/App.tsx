// vendor imports
import { Route } from 'react-router-dom'
// components
import HomePage from 'components/pages/HomePage';
import AddNewContactPage from 'components/pages/AddNewContactPage';
import ContactDetailsPage from 'components/pages/ContactDetailsPage';
// styles
import './App.css';
import 'assets/styles/global.css';
import 'assets/styles/theme.css';

const App: React.FC = (): React.ReactElement => {
    return (
        <>
            <Route exact path='/' component={HomePage} />
            <Route exact path='/addContact' component={AddNewContactPage} />
            <Route exact path='/contactDetails/:id' component={ContactDetailsPage} />
        </>
    );
};

export default App;