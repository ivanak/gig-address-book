// vendor imports
import { combineReducers } from 'redux';
// reducers
import contacts from './contacts';

export default contacts;