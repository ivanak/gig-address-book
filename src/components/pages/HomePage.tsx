// vendor imports
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
// actions
import { setContacts } from 'redux/ducks/contacts';
// types
import { State } from 'redux/ducks/contacts';
// services
import { getLocalStorageContacts } from 'services/localStorage';

const HomePage: React.FC = (): React.ReactElement => {
    const [title, setTitle] = useState('');
    const contacts = useSelector<State, State['contacts']>(state => state.contacts);
    const dispatch = useDispatch();

    useEffect(() => {
        let cList = getLocalStorageContacts();
        dispatch(setContacts(cList));
        if (cList.length > 0) {
            setTitle('Your contacts');
        } else {
            setTitle('Contacts list is empty.');
        }
    }, [dispatch]);

    return (
        <>
            <h1 className='title'>{title}</h1>
            <table>
                <tbody>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Email</th>
                        <th>Country</th>
                        <th>Details</th>
                    </tr>
                    {
                        contacts.length > 0 &&
                        contacts.map(contact => {
                            let { id, firstName, lastName, email, country } = contact;
                            return (
                                <tr key={id}>
                                    <td>{firstName} </td>
                                    <td>{lastName}</td>
                                    <td>{email}</td>
                                    <td>{country}</td>
                                    <td><Link to={`/contactDetails/${id}`}>See details</Link></td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
            <Link className='new-contact-link' to='/addContact'>You can add new contacts here</Link>
        </>
    );
};

export default HomePage;