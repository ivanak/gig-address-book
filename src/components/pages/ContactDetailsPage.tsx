// vendor imports
import React from 'react';
import { Link } from 'react-router-dom';
// components
import ContactDetailsForm from 'components/widgets/Forms/ContactDetailsForm';
// styles
import './style.css';

const ContactDetailsPage: React.FC = (): React.ReactElement =>  {
    return (
        <>
            <ContactDetailsForm />
            <Link to='/'>Back</Link>
        </>
    );
};

export default ContactDetailsPage;