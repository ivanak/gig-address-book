export const VALIDATION_ERROR_MSGS_FIRST_NAME = {
	'REQUIRED': 'First name is required.'
};

export const VALIDATION_ERROR_MSGS_LAST_NAME = {
	'REQUIRED': 'Last name is required.'
};

export const VALIDATION_ERROR_MSGS_EMAIL = {
	'REQUIRED': 'Email is required.',
	'EMAIL_ADDRESS': 'You have entered an invalid email address.'
};

export const VALIDATION_ERROR_MSGS_COUNTRY = {
	'REQUIRED': 'Country is required.'
};