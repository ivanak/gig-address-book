// vendor imports
import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
// components
import Input from 'components/ui/Input';
import Button from 'components/ui/Button';
import Select from 'components/ui/Select';
// actions
import { setContacts } from 'redux/ducks/contacts';
// services
import { getLocalStorageContacts, setLocalStorageContacts, checkLocalStorageForEmailDuplicatesOnUpdate } from 'services/localStorage';
// schemas
import { ADD_NEW_CONTACT_SCHEMA } from 'data/schemas/addContact';
// styles
import './style.css';

type ID = {
    id: string;
};

const ContactDetailsForm: React.FC = (): React.ReactElement => {
    const params: ID = useParams();
    const dispatch = useDispatch();
    const [title, setTitle] = useState('');
    const [contactDetails, setContactDetails] = useState({
        firstName: '',
        lastName: '',
        email: '',
        country: ''
    });
    const [contactDeleted, setContactDeleted] = useState(false);
    let formik = useFormik({
        initialValues: {
            firstName: contactDetails.firstName,
            lastName: contactDetails.lastName,
            email: contactDetails.email,
            country: contactDetails.country
        },
        validationSchema: ADD_NEW_CONTACT_SCHEMA,
        validateOnChange: false,
        validateOnBlur: false,
        enableReinitialize: true,
        onSubmit: () => {
            updateMyContact();
        }
    });

    useEffect(() => {
        let contacts = getLocalStorageContacts();
        if (contacts.length > 0) {
            let currentContact = contacts.filter(contact => contact.id === params.id);
            if (currentContact.length > 0) {
                setContactDetails(currentContact[0]);
            } else {
                setContactDeleted(true);
            }
        }
    }, [params.id]);

    const onSubmitForm = (): void => {
        formik.handleSubmit();
    };

    const updateMyContact = (): void => {
        let contact = {
            id: params.id,
            firstName: formik.values.firstName,
            lastName: formik.values.lastName,
            email: formik.values.email,
            country: formik.values.country
        };
        if (!checkLocalStorageForEmailDuplicatesOnUpdate(contact)) {
            let contacts = getLocalStorageContacts();
            let updatedContacts = contacts.map(c => {
                if (c.id === params.id) {
                    return contact;
                } else {
                    return c;
                }
            });
            dispatch(setContacts(updatedContacts));
            setLocalStorageContacts(updatedContacts);
            setTitle('Contact successfully updated.');

        } else {
            setTitle('Contact with that email address already exists.');
        }
        resetTitle();
    };

    const resetTitle = (): void => {
        setTimeout(() => {
            setTitle('');
        }, 3000);
    };

    const deleteMyContact = (): void => {
        let contacts = getLocalStorageContacts();
        let updatedContacts = contacts.filter(c => c.id !== params.id);
        dispatch(setContacts(updatedContacts));
        setLocalStorageContacts(updatedContacts);
        setTitle('Contact successfully deleted.');
        resetTitle();
        setContactDeleted(true);
    };

    return (
        <div className='form-wrapper'>
            {contactDeleted
                ?
                <h1 className='title'>Contact deleted</h1>
                :
                <>
                    <h1 className='title'>{title}</h1>
                    <h1 className='title'>Contact details</h1>
                    <Input
                        label='First name'
                        placeholder='First name'
                        name='firstName'
                        value={formik.values.firstName}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.firstName ? <div className='error'>{formik.errors.firstName}</div> : null}
                    <Input
                        label='Last name'
                        placeholder='Last name'
                        name='lastName'
                        value={formik.values.lastName}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.lastName ? <div className='error'>{formik.errors.lastName}</div> : null}
                    <Input
                        label='Email'
                        placeholder='Email'
                        name='email'
                        value={formik.values.email}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.email ? <div className='error'>{formik.errors.email}</div> : null}
                    <Select
                        label='Country'
                        value={formik.values.country}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.country ? <div className='error'>{formik.errors.country}</div> : null}
                    <div className='contact-actions'>
                        <Button
                            label='Update contact'
                            type='primary'
                            onClick={onSubmitForm}
                        />
                        <Button
                            label='Delete contact'
                            type='delete'
                            onClick={deleteMyContact}
                        />
                    </div>
                </>
            }
        </div>
    );
};

export default ContactDetailsForm;