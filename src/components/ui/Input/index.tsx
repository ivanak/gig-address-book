// vendor imports
import React from 'react';
// styles
import './style.css';

type Props = {
    label: string;
    placeholder: string;
    name: string;
    value: string;
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

const Input: React.FC<Props> = ({ label, placeholder, name, value, onChange }): React.ReactElement => {
    return (
        <div className='input-group'>
            <span className='input-group__label'>{label}</span>
            <input
                autoComplete='off'
                className='input-group__input'
                placeholder={placeholder}
                type='text'
                name={name}
                value={value}
                onChange={onChange}
            />
        </div>
    );
};

export default Input;