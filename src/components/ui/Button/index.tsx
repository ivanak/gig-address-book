// vendor imports
import React from 'react';
// styles
import './style.css';

type Props = {
    label: string;
    type: string;
    onClick: React.MouseEventHandler<HTMLButtonElement>;
};

const Button: React.FC<Props> = ({ label, type, onClick }): React.ReactElement => {
    return (
        <button className={`button button-${type}`} onClick={onClick} type='submit'>
            {label}
        </button>
    );
};

export default Button;