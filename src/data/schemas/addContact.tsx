// vendor imports 
import * as Yup from 'yup';
// constants
import {
    VALIDATION_ERROR_MSGS_FIRST_NAME,
    VALIDATION_ERROR_MSGS_LAST_NAME,
    VALIDATION_ERROR_MSGS_EMAIL,
    VALIDATION_ERROR_MSGS_COUNTRY
} from '../validationErrorMsgs';

export const ADD_NEW_CONTACT_SCHEMA = Yup.object({
    firstName: Yup.string()
        .required(VALIDATION_ERROR_MSGS_FIRST_NAME.REQUIRED),
    lastName: Yup.string()
        .required(VALIDATION_ERROR_MSGS_LAST_NAME.REQUIRED),
    email: Yup.string()
        .email(VALIDATION_ERROR_MSGS_EMAIL.EMAIL_ADDRESS)
        .required(VALIDATION_ERROR_MSGS_EMAIL.REQUIRED),
    country: Yup.string()
        .required(VALIDATION_ERROR_MSGS_COUNTRY.REQUIRED)
});