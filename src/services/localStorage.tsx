// types
import { Contact } from 'redux/ducks/contacts';

export const getLocalStorageContacts = (): Contact[] => {
    let contacts = localStorage.getItem('contacts');
    if (contacts) {
        return JSON.parse(contacts);
    } else {
        return [];
    }
};

export const setLocalStorageContacts = (contacts: Contact[]): void => {
    localStorage.setItem('contacts', JSON.stringify(contacts));
};

export const checkLocalStorageForEmailDuplicates = (email: string): boolean => {
    let contacts = localStorage.getItem('contacts');
    if (contacts) {
        let cList: Contact[] = JSON.parse(contacts);
        let duplicateContacts: Contact[] = cList.filter(contact => contact.email === email);
        return duplicateContacts.length > 0 ? true : false;
    } else {
        return false;
    }
};

export const checkLocalStorageForEmailDuplicatesOnUpdate = (contact: Contact): boolean => {
    let contacts = localStorage.getItem('contacts');
    if (contacts) {
        let cList: Contact[] = JSON.parse(contacts);
        let contactDetails: Contact[] = cList.filter(c => c.id === contact.id);
        let duplicateContacts: Contact[] = cList.filter(c => c.email === contact.email);
        return (contactDetails[0].email !== contact.email) && (duplicateContacts.length > 0) ? true : false ;
    } else {
        return false;
    }
};