// vendor imports
import React from 'react';
import { Link } from 'react-router-dom';
// components
import NewContactForm from 'components/widgets/Forms/NewContactForm';
// styles
import './style.css';

const AddNewContactPage: React.FC = (): React.ReactElement => {
    return (
        <>
            <NewContactForm />
            <Link to='/'>See all contacts</Link>
        </>
    );
};

export default AddNewContactPage;