// vendor imports
import React, { useState } from 'react';
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
// components
import Input from 'components/ui/Input';
import Button from 'components/ui/Button';
import Select from 'components/ui/Select';
// actions
import { addContact } from 'redux/ducks/contacts';
// types
import { Contact } from 'redux/ducks/contacts';
// services
import { getLocalStorageContacts, setLocalStorageContacts, checkLocalStorageForEmailDuplicates } from 'services/localStorage';
// schemas
import { ADD_NEW_CONTACT_SCHEMA } from 'data/schemas/addContact';
// styles
import './style.css';

const NewContactForm: React.FC = (): React.ReactElement => {
    const dispatch = useDispatch();
    const [title, setTitle] = useState('');
    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
            country: ''
        },
        validationSchema: ADD_NEW_CONTACT_SCHEMA,
        validateOnChange: false,
        validateOnBlur: false,
        onSubmit: () => {
            addMyContact();
        }
    });

    const onSubmitForm = (): void => {
        formik.handleSubmit();
    };

    const addMyContact = (): void => {
        let contact = {
            id: Math.random().toString(36).substr(2, 9),
            firstName: formik.values.firstName,
            lastName: formik.values.lastName,
            email: formik.values.email,
            country: formik.values.country
        };
        if (!checkLocalStorageForEmailDuplicates(contact.email)) {
            dispatch(addContact(contact));
            addContactInLocalStorage(contact);
            setTitle('Contact successfully added.');
            formik.resetForm();
        } else {
            setTitle('Contact with that email address already exists.');
        }
        resetTitle();
    };

    const addContactInLocalStorage = (contact: Contact): void => {
        let contacts = getLocalStorageContacts();
        contacts.push(contact);
        setLocalStorageContacts(contacts);
    };

    const resetTitle = (): void => {
        setTimeout(() => {
            setTitle('');
        }, 3000);
    };

    return (
        <div className='form-wrapper'>
            <h1 className='title'>{title}</h1>
            <h1 className='title'>Your new contact</h1>
            <Input
                label='First name'
                placeholder='First name'
                name='firstName'
                value={formik.values.firstName}
                onChange={formik.handleChange}
            />
            {formik.errors.firstName ? <div className='error'>{formik.errors.firstName}</div> : null}
            <Input
                label='Last name'
                placeholder='Last name'
                name='lastName'
                value={formik.values.lastName}
                onChange={formik.handleChange}
            />
            {formik.errors.lastName ? <div className='error'>{formik.errors.lastName}</div> : null}
            <Input
                label='Email'
                placeholder='Email'
                name='email'
                value={formik.values.email}
                onChange={formik.handleChange}
            />
            {formik.errors.email ? <div className='error'>{formik.errors.email}</div> : null}
            <Select
                label='Country'
                value={formik.values.country}
                onChange={formik.handleChange}
            />
            {formik.errors.country ? <div className='error'>{formik.errors.country}</div> : null}
            <Button
                label='Add new contact'
                type='primary'
                onClick={onSubmitForm}
            />
        </div>
    );
};

export default NewContactForm;