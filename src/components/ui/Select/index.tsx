// vendor imports
import React from 'react';
import countries from 'country-list';
// styles
import './style.css';

// country list constant
const COUNTRY_LIST = countries.getData();

type Props = {
    label: string;
    value: string;
    onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void;
};

const Select: React.FC<Props> = ({ label, value, onChange }): React.ReactElement => {
    return (
        <div>
            <span className='select__label'>{label}</span>
            <select name='country' value={value} onChange={onChange}>
                {
                    COUNTRY_LIST.map(country => {
                        return (
                            <option key={country.code} value={country.name}>{country.name}</option>
                        )
                    })
                }
            </select>
        </div>
    );
};

export default Select;