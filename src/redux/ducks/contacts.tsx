// types
export type Contact = {
    id: string,
    firstName: string;
    lastName: string;
    email: string;
    country: string;
};

export type State = {
    contacts: Contact[]
};

type SetContactsAction = {
    type: string;
    payload: Contact[];
};

type AddContactAction = {
    type: string;
    payload: Contact;
};

type UpdateContactAction = {
    type: string;
    payload: Contact;
};

type DeleteContactAction = {
    type: string;
    payload: string;
};

// type Action = SetContactsAction | AddContactAction | UpdateContactAction | DeleteContactAction;
type Action = {
    type: string;
    payload: Contact[];
};

// initial state
const init: State = {
    contacts: []
};

// constants
const SET_CONTACTS = 'SET_CONTACTS';
const UPDATE_CONTACT = 'UPDATE_CONTACT';
const DELETE_CONTACT = 'DELETE_CONTACT';

// actions
export const setContacts = (contacts: Contact[]): SetContactsAction => {
    return {
        type: SET_CONTACTS,
        payload: contacts
    };
};

export const addContact = (contact: Contact): AddContactAction => {
    return {
        type: SET_CONTACTS,
        payload: contact
    };
};

export const updateContact = (contact: Contact): UpdateContactAction => {
    return {
        type: UPDATE_CONTACT,
        payload: contact
    };
};

export const deleteContact = (id: string): DeleteContactAction => {
    return {
        type: DELETE_CONTACT,
        payload: id
    };
};

function setContactsState(state: State, action: Action): State {
    if (Array.isArray(action.payload)) {
        return {
            ...state, contacts: action.payload
        }
    } else if (typeof action.payload === 'object') {
        return {
            ...state, contacts: [...state.contacts, action.payload]
        }
    } else {
        return state;
    }
};

// reducer
const reducer = (state: State = init, action: Action): State => {
    switch (action.type) {
        case SET_CONTACTS:
            return {
                ...state, contacts: action.payload
            }
        default:
            return state;
    };
};

export default reducer; 